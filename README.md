# docs

#### 介绍
docs是OpenHarmony三方组件库私有仓组织的一个文档仓，存放OpenHarmony三方组件子项目文档。

#### 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/docs/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-tpc/docs/pulls) 。

#### 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-tpc/docs/blob/master/LICENSE) ，请自由地享受和参与开源。
